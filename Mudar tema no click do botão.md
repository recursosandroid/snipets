Faz um projeto novo e utiliza o template `Blank Activity`
Adiciona esses temas modificados ao arquivo `res/values/style.xml`

```xml 
<style name="TemaVerde" parent="Theme.AppCompat.Light">
        <!-- Customize your theme here. -->
        <item name="colorPrimary">#66bb6a</item>
        <item name="colorPrimaryDark">#338a3e</item>
        <item name="colorAccent">#bb6766</item>
    </style>

    <style name="TemaAmarelo" parent="ThemeOverlay.AppCompat.Dark">
        <!-- Customize your theme here. -->
        <item name="colorPrimary">#ffeb3b</item>
        <item name="colorPrimaryDark">#c8b900</item>
        <item name="colorAccent">#d83bff</item>
    </style>
```

Adiciona isso ao layout `res/layout/activity_main.xml`

```xml
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <Button
        android:id="@+id/botaoamarelo"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginTop="8dp"
        android:text="Tema Amarelo"
        app:layout_constraintEnd_toStartOf="@+id/botaoverde"
        app:layout_constraintHorizontal_bias="0.5"
        app:layout_constraintHorizontal_chainStyle="packed"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

    <Button
        android:id="@+id/botaoverde"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginLeft="16dp"
        android:layout_marginStart="16dp"
        android:layout_marginTop="8dp"
        android:text="Tema Verde"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.5"
        app:layout_constraintStart_toEndOf="@+id/botaoamarelo"
        app:layout_constraintTop_toTopOf="parent" />

    <TextView
        android:id="@+id/textView"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginEnd="8dp"
        android:layout_marginLeft="8dp"
        android:layout_marginRight="8dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="32dp"
        android:text="Hello World!"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/botaoamarelo" />
</android.support.constraint.ConstraintLayout>
```

Modifique o conteúdo do `onCreate()` dentro do seu `MainActivity()`

```java
 @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean corTema = prefs.getBoolean("cor_verde",false);

        if(corTema) {  setTheme(R.style.TemaVerde);
        } else {  setTheme(R.style.TemaAmarelo); }

        setContentView(R.layout.activity_main);

        final SharedPreferences.Editor editor = prefs.edit();

        //definir e ação dos botões
        Button verde = findViewById(R.id.botaoverde);
        verde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("cor_verde",true);
                editor.commit();
                recreate(); //recria a atividade
             } });

        Button amarelo = findViewById(R.id.botaoamarelo);
        amarelo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("cor_verde",false);
                editor.commit();
                recreate();
             } });

    }
```