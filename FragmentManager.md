Ao seu layout onde quer trocar fragmentos adicione um `fragmelayout`

```xml
 <FrameLayout
            android:id="@+id/containerFragmentos"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            app:layout_behavior="@string/appbar_scrolling_view_behavior">

        </FrameLayout>
```


Adicione um objeto à classe 

```java
    FragmentManager fragmentManager;
```

Adicione um fragmento inicial ao `onCreate()`

```java
fragmentManager = getSupportFragmentManager();

        Inicial fragInicial = new Inicial();

        fragmentManager.beginTransaction().add(R.id.containerFragmentos,fragInicial).commit();
```

Adicione este método à classe pra trocar fragmentos

```java
 private void substituirFragmento(Fragment fragmento, @Nullable String fragKey, @Nullable Bundle bundle) {

        if(bundle!=null) fragmento.setArguments(bundle);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.containerFragmentos,fragmento,fragKey);
        fragmentTransaction.commit();
    }
```

Pode adicionar fragmentos
Botão direito -> New -> Fragment -> Blank Fragment