# Requerir permissão única

Exemplo pra pedir permissão pra uso da câmera (necessário permissão durante runtime pra API >= 23)

## Com library

https://github.com/Karumi/Dexter

(para requisição única de permissão e para diversas requisições de permissão)

## Sem library


Na atividade:

```java
    private final int PERMISSAO_USAR_CAMERA = 1;

//chamar dentro do onCreate()
//checa pra ver se a versão é maior ou igual a marshmallow (api 23)
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checarEPedirPermissoes(this);
        }     
```

Métodos 

```java
private void checarEPedirPermissoes(Context context) {

        int checarPermissaoCamera = ContextCompat.checkSelfPermission(
                this, Manifest.permission.CAMERA);

        if (checarPermissaoCamera!= PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.CAMERA)) {

                //mostrar explicação para utilizar a permissão
                mostrarExplicacaoParaPermissao("Permissao Para utilizar a câmera",
                        "Tirar foto para o exemplo",
                        Manifest.permission.CAMERA,PERMISSAO_USAR_CAMERA);
            } else {
                //
                requerirPermissao(Manifest.permission.CAMERA,PERMISSAO_USAR_CAMERA);
            }
        } else {
            Toast.makeText(this,"Permissão já concedida!",Toast.LENGTH_SHORT).show();
        }
    }

    private void mostrarExplicacaoParaPermissao(String titulo,
                                 String mensagem,
                                 final String permissao,
                                 final int codigoRequisicaoPermissao) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo)
                .setMessage(mensagem)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requerirPermissao(permissao, codigoRequisicaoPermissao);
                    }
                });
        builder.create().show();
    }

    private void requerirPermissao(String nomePermissao, int codigoRequisicaoPermissao) {
        ActivityCompat.requestPermissions(this,
                new String[]{nomePermissao}, codigoRequisicaoPermissao);
    }
```
